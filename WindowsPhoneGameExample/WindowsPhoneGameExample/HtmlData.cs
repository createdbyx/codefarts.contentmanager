﻿namespace SilverlightExample
{
    /// <summary>
    /// Provides a custom html data type.
    /// </summary>
    public class HtmlData
    {
        /// <summary>
        /// Gets or sets the html markup.
        /// </summary>
        public string Markup { get; set; }
    }
}